# Clenshaw

Clenshaw is a mixed C and python library that provides solving and plotting
function of polynomial in the Taylor or the Chebyshev basis.  The library
is optimized for machine double precision and for numerically
well-conditioned polynomials.

## Installation

Clenshaw depends on numpy, flint-py, and optionally on scipy.fftpack, scikit-image and visvis.

After cloning the repository, clenshaw can be installed with:

    sudo python3 setup.py install

flint-py includes flint and arb which are used to manipulate multi-precision
number when required.  Optionally, scipy.fftpack provides approximate discrete
cosine transform for the functions `evalgrid_idct` and `poly2cheb_dct`.
Skimage and visvis provides marching cube algorithm and visualisation of
curves and surfaces in the function `implicitplot`.

Furthermore, it is required that the CPU running the library is IEEE 754
compliant.

## Examples

### Root isolation

The following example illustrates how to isolate the solutions of a high
degree polynomial

    import clenshaw as cs
    import numpy as np
    np.random.seed(0)
    p = np.random.randn(100000)
    sols, unks = cs.solve_polynomial_taylor(p)

The variable `sols` is the array of intervals that contain exactly one solution.
and `unks` is the array of intervals that may contain a solution. Moreover,
all the solutions of `p` are contained in the union of intervals in `sols`
and `unks`. In this case, `unks` is empty, which means that the all the
solutions have been found. The variable `sols` is:

    array([[-9.85451221e-01,  2.14576721e-05],
           [ 9.96135712e-01,  8.58306885e-06],
           [ 9.99992371e-01,  7.62939453e-06],
           [-1.00006295e+00,  1.33560970e-05],
           [-1.00607608e+00,  9.89809632e-06],
           [-1.62229288e+00,  4.15770337e-05],
           [ 1.00239659e+00,  1.62934884e-05]])

Each interval is represented by two double : the center and the radius of
the interval.

The followind example isolates the roots of a polynomial expressed in the
chebyshev basis in the interval `[-1, 1]` :

    import clenshaw as cs
    import numpy as np
    np.random.seed(0)
    p = np.random.randn(10000)
    sols, unks = cs.solve_polynomial_chebyshev(p)

The variable `unks` is empty, such that all the solutions have been found.
The variable `sols` is an array of 11576 intervals, including:

    array([[-9.99999762e-01,  1.19209290e-07],
           [-9.99998510e-01,  1.78813934e-07],
           [-9.99996901e-01,  2.38418579e-07],
           ...,
           [ 9.99999583e-01,  5.96046448e-08],
           [ 9.99999873e-01,  2.23517418e-08],
           [ 9.99999955e-01,  1.49011612e-08]])


### Plotting a high degree surface (experimental)

Plotting a random surface of degree 50 in the window with lower corner
(0,0,0) and upper corner (2,2,2).

    import clenshaw as cs
    import numpy as np
    np.random.seed(0)
    n = 50
    p = np.random.randn(n,n,n)
    cs.implicitplot(p, (0,0,0), (2,2,2))
    
Which result in the following 3D plot.

[![Screenshot of a surface of degree 50](surface.jpg)](surface.png)


Plotting functions are not reliable. The precision can be increased with
the option `precision=256` for example. Default precision is 128 for 3
variables and 512 for 2 variables. Different methods can also be used with the
options `method='dct_scale'` (default), `method='dct'`,
`method='taylorshift'` or `method='horner'`.

