import clenshaw as cs
import numpy as np
import random
import time

deg = [128, 181, 256, 362, 512, 724, 1024, 1448, 2048, 2896, 4096, 5792, 8192, 11585, 16384, 23170, 32768]


random.seed(0)
L = []
for d in deg:
    print(d)
    p = np.array([random.randint(-2**1024, 2**1024) for k in range(d)])
    start = time.perf_counter()
    sols, unks = cs.solve_polynomial_taylor(p)
    dt = time.perf_counter() - start
    L += [dt]
    print(unks)
    


