import clenshaw as cs
import numpy as np

# should return 2 unknown solutions around 1
cs.solve_polynomial([-1, 0, 0, 0, 0, 1])
cs.solve_polynomial([-1] + [0]*100 + [1])


# clenshaw eval at 1 should contain 0
f = np.array([-1] + [0]*6 + [1.])
ct = np.zeros_like(f)
r  = np.zeros(1)
cs.chebyshev(f, ct, r)
cs.clenshaw(ct, np.ones(1), np.zeros(1))
