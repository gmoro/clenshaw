import clenshaw as cs
import flint as fl
import random
W = fl.fmpz_poly([fl.fmpz(random.randint(-2**1031, 2**1031)) for i in range(4096)])
Vi = W*W-1
Vz = list(int(e) for e in Vi)
#Vi = W*W+1
Va = np.array([fl.arb(e) for e in Vi])
Vn = Va/Va.max()
# compute f
Vf = Vn.astype(float)
f = np.zeros_like(Vf)
err_f = np.zeros(1)
cs.chebyshev(Vf, f, err_f)
err_f += sum(float(e.rad()) for e in Vn)
# compute df
dVn = Vn*np.arange(Vn.size)
Vdf = dVn.astype(float)
df = np.zeros_like(Vdf)
err_df = np.zeros(1)
cs.chebyshev(Vdf, df, err_df)
err_df += sum(float(e.rad()) for e in dVn)
# solve
cs.solve(f, df, err_f, err_df, 5)



