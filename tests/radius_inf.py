import numpy as np
import clenshaw as cs

V = np.random.randn(50000)
qru = np.zeros_like(V)
r = np.zeros(1)
cs.chebyshev(V, qru[:37000], r)
