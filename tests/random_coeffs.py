import clenshaw as cs
import numpy as np
import flint as fl
import random
n = 100000
W = fl.fmpz_poly([fl.fmpz(random.randint(-2**1024, 2**1024)) for i in range(n)]+[fl.fmpz(1)])
#W = fl.fmpz_poly([fl.fmpz(random.randint(-2**1024, 2**1024)) for i in range(n)])
Va = np.array([fl.arb(e) for e in W])
Vn = Va/Va.max()
# compute f
Vf = Vn.astype(float)
f = np.zeros_like(Vf)
err_f = np.zeros(1)
cs.chebyshev(Vf, f, err_f)
f = np.trim_zeros(f, 'b')
err_f += sum(float(e.rad()) for e in Vn)
# compute df
dVn = Vn*np.arange(Vn.size)
Vdf = dVn.astype(float)
df = np.zeros_like(Vdf)
err_df = np.zeros(1)
cs.chebyshev(Vdf, df, err_df)
df = np.trim_zeros(df, 'b')
err_df += sum(float(e.rad()) for e in dVn)
# solve
cs.solve(f, df, err_f, err_df)



