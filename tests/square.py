import numpy as np
import clenshaw as cs

f = np.zeros(3)
err_f = np.zeros(1)
df = np.zeros(2)
err_df = np.zeros(1)
cs.chebyshev(np.array([0.5**10,0,1]), f, err_f)
cs.chebyshev(np.array([0.,2]), df, err_df)
cs.solve(f, df, err_f, err_df) # should return no sols


