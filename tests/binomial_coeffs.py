import clenshaw as cs
import numpy as np
np.random.seed(0)
p = np.random.randn(27)
q = np.vectorize(lambda x: np.math.comb(p.size-1,x), otypes=(object,))(np.arange(p.size))*p
cs.solve_polynomial_taylor(q)
# returns inf
