import clenshaw as cs
import numpy as np
import sympy as sp
from sympy.abc import x,y,z

# Computing discriminant with sympy
n = 5
np.random.seed(0)
p = sum(np.random.randint(-100,100)*x**i*y**j*z**k
        for i in range(n) for j in range(n-i) for k in range(n-i-j))
q = sp.discriminant(p, z).as_poly()

# Converting and plotting
r = np.zeros(tuple(d+1 for d in q.degree_list()))
for d, c in q.terms():
    r[d] = c
cs.implicitplot(r)

