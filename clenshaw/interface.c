/****************************************************************************
        Copyright (C) 2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "Python.h"
#include "numpy/ndarrayobject.h"
#include "clenshaw.h"

static PyObject* horner(PyObject* self, PyObject* args)
{
    PyObject *arg1=NULL, *arg2=NULL;
    PyArrayObject *coefficients, *values, *radius=NULL;
    //TODO: check that input args are 2 arrays
    //if(!PyArg_ParseTuple(args, "O!O!", &PyArray_Type, &arg1, &PyArray_Type, &arg2)) {
    //TODO: handle optional third array
    if(!PyArg_ParseTuple(args, "OO|O", &arg1, &arg2, &radius)) {
        return NULL;
    }
    coefficients = (PyArrayObject*) arg1;
    values = (PyArrayObject*) arg2;
    int Dc = PyArray_NDIM(coefficients);
    int Dv = PyArray_NDIM(values);

    if(!PyArray_ISCARRAY_RO(coefficients)) {
        PyErr_SetString(PyExc_TypeError, "First array should have a C contiguous layout");
        return NULL;
    }
    if(!PyArray_ISCARRAY(values)) {
        PyErr_SetString(PyExc_TypeError, "Second array should have a C contiguous layout");
        return NULL;
    }
    if((Dc != Dv) || (Dc<1) || (Dv<1) ) {
        PyErr_SetString(PyExc_TypeError, "Array sizes don't match");
        return NULL;
    }
    if(PyArray_DESCR(coefficients)->type_num != PyArray_DESCR(values)->type_num) {
        PyErr_SetString(PyExc_TypeError, "Array types don't match");
        return NULL;
    }
    
    npy_intp Nc = PyArray_DIM(coefficients, Dc-1);
    npy_intp Nv = PyArray_DIM(values, Dv-1);
    npy_intp Pc = 1;
    for(int i=0; i < Dc-1; i++) {
        Pc *= PyArray_DIM(coefficients, i);
    }
    npy_intp Pv = 1;
    for(int i=0; i < Dv-1; i++) {
        Pv *= PyArray_DIM(values, i);
    }

    if(Pv != Pc) {
        PyErr_SetString(PyExc_TypeError, "Input shapes don't match");
        return NULL;
    }

    if(radius != NULL) {
        int Dr = PyArray_NDIM(radius);
        npy_intp Nr = PyArray_DIM(radius, Dr-1);
        if(!PyArray_ISCARRAY(radius)) {
            PyErr_SetString(PyExc_TypeError, "Third array should have a C contiguous layout");
            return NULL;
        }
        if((Dc != Dr) || (Dr<1) || (Nv != Nr)) {
            PyErr_SetString(PyExc_TypeError, "Array sizes don't match");
            return NULL;
        }
        if(PyArray_DESCR(coefficients)->type_num != PyArray_DESCR(radius)->type_num) {
            PyErr_SetString(PyExc_TypeError, "Array types don't match");
            return NULL;
        }
        npy_intp Pr = 1;
        for(int i=0; i < Dr-1; i++) {
            Pr *= PyArray_DIM(radius, i);
        }
        if(Pr != Pc) {
            PyErr_SetString(PyExc_TypeError, "Input shapes don't match");
            return NULL;
        }
    }

    if(PyArray_DESCR(coefficients)->type_num == NPY_FLOAT32) {
        if(radius == NULL) {
            for(npy_intp i=0; i < Pc; i++) {
                s_horner((float*) PyArray_DATA(values) + i*Nv, Nv,
                         (float*) PyArray_DATA(coefficients) + i*Nc, Nc);
            }
        } else {
            for(npy_intp i=0; i < Pc; i++) {
                sb_horner((float*) PyArray_DATA(values) + i*Nv,
                          (float*) PyArray_DATA(radius) + i*Nv, Nv,
                          (float*) PyArray_DATA(coefficients) + i*Nc, Nc);
            }
        }
    } else if(PyArray_DESCR(coefficients)->type_num == NPY_FLOAT64) {
        if(radius == NULL) {
            for(npy_intp i=0; i < Pc; i++) {
                d_horner((double*) PyArray_DATA(values) + i*Nv, Nv,
                         (double*) PyArray_DATA(coefficients) + i*Nc, Nc);
            }
        } else {
            for(npy_intp i=0; i < Pc; i++) {
                db_horner((double*) PyArray_DATA(values) + i*Nv,
                          (double*) PyArray_DATA(radius) + i*Nv, Nv,
                          (double*) PyArray_DATA(coefficients) + i*Nc, Nc);
            }
        }
    } else {
        PyErr_SetString(PyExc_TypeError, "Array types should be either float32 or float64");
        return NULL;
    }
    if(radius == NULL) {
        Py_INCREF(arg2);
        return arg2;
    } else {
        return Py_BuildValue("(OO)", arg2, radius);
    }
}

static PyObject* clenshaw(PyObject* self, PyObject* args)
{
    PyObject *arg1=NULL, *arg2=NULL;
    PyArrayObject *coefficients, *values, *radius=NULL;
    //TODO: check that input args are 2 arrays
    //if(!PyArg_ParseTuple(args, "O!O!", &PyArray_Type, &arg1, &PyArray_Type, &arg2)) {
    //TODO: handle optional third array
    if(!PyArg_ParseTuple(args, "OO|O", &arg1, &arg2, &radius)) {
        return NULL;
    }
    coefficients = (PyArrayObject*) arg1;
    values = (PyArrayObject*) arg2;
    int Dc = PyArray_NDIM(coefficients);
    int Dv = PyArray_NDIM(values);

    if(!PyArray_ISCARRAY_RO(coefficients)) {
        PyErr_SetString(PyExc_TypeError, "First array should have a C contiguous layout");
        return NULL;
    }
    if(!PyArray_ISCARRAY(values)) {
        PyErr_SetString(PyExc_TypeError, "Second array should have a C contiguous layout");
        return NULL;
    }
    if((Dc != Dv) || (Dc<1) || (Dv<1) ) {
        PyErr_SetString(PyExc_TypeError, "Array sizes don't match");
        return NULL;
    }
    if(PyArray_DESCR(coefficients)->type_num != PyArray_DESCR(values)->type_num) {
        PyErr_SetString(PyExc_TypeError, "Array types don't match");
        return NULL;
    }
    
    npy_intp Nc = PyArray_DIM(coefficients, Dc-1);
    npy_intp Nv = PyArray_DIM(values, Dv-1);
    npy_intp Pc = 1;
    for(int i=0; i < Dc-1; i++) {
        Pc *= PyArray_DIM(coefficients, i);
    }
    npy_intp Pv = 1;
    for(int i=0; i < Dv-1; i++) {
        Pv *= PyArray_DIM(values, i);
    }

    if(Pv != Pc) {
        PyErr_SetString(PyExc_TypeError, "Input shapes don't match");
        return NULL;
    }

    if(radius != NULL) {
        int Dr = PyArray_NDIM(radius);
        npy_intp Nr = PyArray_DIM(radius, Dr-1);
        if(!PyArray_ISCARRAY(radius)) {
            PyErr_SetString(PyExc_TypeError, "Third array should have a C contiguous layout");
            return NULL;
        }
        if((Dc != Dr) || (Dr<1) || (Nv != Nr)) {
            PyErr_SetString(PyExc_TypeError, "Array sizes don't match");
            return NULL;
        }
        if(PyArray_DESCR(coefficients)->type_num != PyArray_DESCR(radius)->type_num) {
            PyErr_SetString(PyExc_TypeError, "Array types don't match");
            return NULL;
        }
        npy_intp Pr = 1;
        for(int i=0; i < Dr-1; i++) {
            Pr *= PyArray_DIM(radius, i);
        }
        if(Pr != Pc) {
            PyErr_SetString(PyExc_TypeError, "Input shapes don't match");
            return NULL;
        }
    }

    if(PyArray_DESCR(coefficients)->type_num == NPY_FLOAT32) {
        if(radius == NULL) {
            for(npy_intp i=0; i < Pc; i++) {
                s_clenshaw((float*) PyArray_DATA(values) + i*Nv, Nv,
                           (float*) PyArray_DATA(coefficients) + i*Nc, Nc);
            }
        } else {
            for(npy_intp i=0; i < Pc; i++) {
                sb_clenshaw((float*) PyArray_DATA(values) + i*Nv,
                            (float*) PyArray_DATA(radius) + i*Nv, Nv,
                            (float*) PyArray_DATA(coefficients) + i*Nc, Nc);
            }
        }
    } else if(PyArray_DESCR(coefficients)->type_num == NPY_FLOAT64) {
        if(radius == NULL) {
            for(npy_intp i=0; i < Pc; i++) {
                d_clenshaw((double*) PyArray_DATA(values) + i*Nv, Nv,
                           (double*) PyArray_DATA(coefficients) + i*Nc, Nc);
            }
        } else {
            for(npy_intp i=0; i < Pc; i++) {
                db_clenshaw((double*) PyArray_DATA(values) + i*Nv,
                            (double*) PyArray_DATA(radius) + i*Nv, Nv,
                            (double*) PyArray_DATA(coefficients) + i*Nc, Nc);
            }
        }
    } else {
        PyErr_SetString(PyExc_TypeError, "Array types should be either float32 or float64");
        return NULL;
    }
    if(radius == NULL) {
        Py_INCREF(arg2);
        return arg2;
    } else {
        return Py_BuildValue("(OO)", arg2, radius);
    }
}

static PyObject* chebyshev(PyObject* self, PyObject* args)
{
    PyObject *arg1=NULL, *arg2=NULL;
    PyArrayObject *coefficients, *values, *radius=NULL;
    //TODO: check that input args are 2 arrays
    //if(!PyArg_ParseTuple(args, "O!O!", &PyArray_Type, &arg1, &PyArray_Type, &arg2)) {
    //TODO: handle optional third array
    if(!PyArg_ParseTuple(args, "OO|O", &arg1, &arg2, &radius)) {
        return NULL;
    }
    coefficients = (PyArrayObject*) arg1;
    values = (PyArrayObject*) arg2;
    int Dc = PyArray_NDIM(coefficients);
    int Dv = PyArray_NDIM(values);

    if(!PyArray_ISCARRAY_RO(coefficients)) {
        PyErr_SetString(PyExc_TypeError, "First array should have a C contiguous layout");
        return NULL;
    }
    if(!PyArray_ISCARRAY(values)) {
        PyErr_SetString(PyExc_TypeError, "Second array should have a C contiguous layout");
        return NULL;
    }
    if((Dc != Dv) || (Dc<1) || (Dv<1) ) {
        PyErr_SetString(PyExc_TypeError, "Array sizes don't match");
        return NULL;
    }
    if(PyArray_DESCR(coefficients)->type_num != PyArray_DESCR(values)->type_num) {
        PyErr_SetString(PyExc_TypeError, "Array types don't match");
        return NULL;
    }
    
    npy_intp Nc = PyArray_DIM(coefficients, Dc-1);
    npy_intp Nv = PyArray_DIM(values, Dv-1);
    npy_intp Pc = 1;
    for(int i=0; i < Dc-1; i++) {
        Pc *= PyArray_DIM(coefficients, i);
    }
    npy_intp Pv = 1;
    for(int i=0; i < Dv-1; i++) {
        Pv *= PyArray_DIM(values, i);
    }

    if(Pv != Pc) {
        PyErr_SetString(PyExc_TypeError, "Input shapes don't match");
        return NULL;
    }

    if(radius != NULL) {
        int Dr = PyArray_NDIM(radius);
        npy_intp Nr = PyArray_DIM(radius, Dr-1);
        if(!PyArray_ISCARRAY(radius)) {
            PyErr_SetString(PyExc_TypeError, "Third array should have a C contiguous layout");
            return NULL;
        }
        if((Dc != Dr) || (Dr<1) || (Nr != 1)) {
            PyErr_SetString(PyExc_TypeError, "Array sizes don't match");
            return NULL;
        }
        if(PyArray_DESCR(coefficients)->type_num != PyArray_DESCR(radius)->type_num) {
            PyErr_SetString(PyExc_TypeError, "Array types don't match");
            return NULL;
        }
        npy_intp Pr = 1;
        for(int i=0; i < Dr-1; i++) {
            Pr *= PyArray_DIM(radius, i);
        }
        if(Pr != Pc) {
            PyErr_SetString(PyExc_TypeError, "Input shapes don't match");
            return NULL;
        }
    }

    if(PyArray_DESCR(coefficients)->type_num == NPY_FLOAT32) {
        if(radius == NULL) {
            for(npy_intp i=0; i < Pc; i++) {
                //s_chebyshev((float*) PyArray_DATA(values) + i*Nv, Nv,
                //            (float*) PyArray_DATA(coefficients) + i*Nc, Nc);
                PyErr_SetString(PyExc_NotImplementedError, "Not implemented.");
                return NULL;
            }
        } else {
            for(npy_intp i=0; i < Pc; i++) {
                //sb_chebyshev((float*) PyArray_DATA(values) + i*Nv,
                //             (float*) PyArray_DATA(radius) + i*Nv, Nv,
                //             (float*) PyArray_DATA(coefficients) + i*Nc, Nc);
                PyErr_SetString(PyExc_NotImplementedError, "Not implemented.");
                return NULL;
            }
        }
    } else if(PyArray_DESCR(coefficients)->type_num == NPY_FLOAT64) {
        if(radius == NULL) {
            for(npy_intp i=0; i < Pc; i++) {
                d_chebyshev((double*) PyArray_DATA(values) + i*Nv, Nv,
                            (double*) PyArray_DATA(coefficients) + i*Nc, Nc);
            }
        } else {
            for(npy_intp i=0; i < Pc; i++) {
                db_chebyshev((double*) PyArray_DATA(values) + i*Nv, Nv,
                             (double*) PyArray_DATA(radius) + i,
                             (double*) PyArray_DATA(coefficients) + i*Nc, Nc);
            }
        }
    } else {
        PyErr_SetString(PyExc_TypeError, "Array types should be either float32 or float64");
        return NULL;
    }
    if(radius == NULL) {
        Py_INCREF(arg2);
        return arg2;
    } else {
        return Py_BuildValue("(OO)", arg2, radius);
    }
}



static PyMethodDef ClenshawMethods[] = {
    {"horner", horner, METH_VARARGS,
     "Evaluate polynomials using a Horner scheme."},
    {"clenshaw", clenshaw, METH_VARARGS,
     "Evaluate polynomials in Chebyshev basis using a Clenshaw scheme."},
    {"chebyshev", chebyshev, METH_VARARGS,
     "Convert polynomials to the Chebyshev basis and truncate."},
    {NULL, NULL, 0, NULL}
};

#if PY_VERSION_HEX >= 0x03000000
static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "_interface",
    NULL,
    -1,
    ClenshawMethods,
    NULL,
    NULL,
    NULL,
    NULL
};

PyMODINIT_FUNC PyInit__interface(void)
{
    PyObject *m;
    m = PyModule_Create(&moduledef);
    if (m == NULL) {
        return NULL;
    }
    return m;
}
#else
PyMODINIT_FUNC init_interface(void)
{
    PyObject *m;
    m = Py_InitModule("_interface", ClenshawMethods);
    if (m == NULL) {
        return;
    }
}
#endif
