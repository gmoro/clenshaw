#*****************************************************************************
#       Copyright (C) 2019 Guillaume Moroz <guillaume.moroz@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

import numpy as np
import itertools
from clenshaw._eval_nd import poly2cheb_dct, poly2cheb_dct_rescale, poly2cheb_taylorshift, evalgrid_idct, to_arb, chebyshev_nodes, evalgrid_horner

def implicitmesh_chebgrid_2d(grid, lower=-1, upper=1, inv=(False,False)):
    from skimage.measure import find_contours
    N = np.array(grid.shape)
    if N.size != 2:
        raise ValueError("The dimension of the ambiant space must be 2")
    if not hasattr(lower, '__iter__'):
        lower = (lower,)
    if not hasattr(upper, '__iter__'):
        upper = (upper,)
    lower = np.append(np.array(lower), (lower[-1],)*(N.size - len(lower)))
    upper = np.append(np.array(upper), (upper[-1],)*(N.size - len(upper)))
    #print("Performing marching square...", flush=True)
    #start_time = time.time()
    verts = find_contours(grid, 0)
    #print("Done in {0}".format(time.time() - start_time))
    C = []
    for contour in verts:
        n = contour.shape[0] * 2 - 2
        #if args.method[0] == 's':
        #    contour = contour[:, [1,0]]
        contour = (upper-lower)*0.5*(1+np.cos(np.pi/N*(0.5+contour))) + lower
        contour[:,inv] = 1/contour[:,inv]
        contour2 = np.empty((n, 2), np.float32)
        contour2[0::2] = contour[:-1]
        contour2[1::2] = contour[1:]
        C.append(contour2)
        #C.append((upper-lower)*0.5*(1+np.cos(np.pi + np.pi/N*(0.5+contour))) + lower)
    #print("Plotting contours...", flush=True)
    #start_time = time.time()
    points = np.row_stack(C)
    return points

def implicitmesh_chebgrid_3d(grid, lower=-1, upper=1, inv=(False,False,False)):
    from skimage.measure import marching_cubes_lewiner
    N = np.array(grid.shape)
    if N.size != 3:
        raise ValueError("The dimension of the ambiant space must be 3")
    if not hasattr(lower, '__iter__'):
        lower = (lower,)
    if not hasattr(upper, '__iter__'):
        upper = (upper,)
    lower = np.append(np.array(lower), (lower[-1],)*(N.size - len(lower)))
    upper = np.append(np.array(upper), (upper[-1],)*(N.size - len(upper)))
    #print("Performing marching cube...", flush=True)
    #start_time = time.time()
    verts, faces, normals, values = marching_cubes_lewiner(grid, 0)
    #print("Done in {0}".format(time.time() - start_time))
    verts = (upper-lower)*0.5*(1+np.cos(np.pi/N*(0.5+verts))) + lower
    verts[:,inv] = 1/verts[:,inv]
    return verts, faces
    #verts = np.cos(np.pi + np.pi/N*(0.5+verts))

def split_1D(l, u, epsilon=0, n=1):
    e = lambda i: epsilon if i < n-1 else 0
    return [(l + i/float(n)*(u-l), l + (i+1)/float(n)*(u-l) + e(i)) for i in range(int(n))]

def split_nD(lower, upper, epsilon=None, N=None):
    L = []
    if epsilon is None:
        epsilon = tuple(0 for i in lower)
    if N is None:
        N = tuple(1 for i in lower)
    for (l,u,e,n) in zip(lower, upper, epsilon, N):
        L.append(split_1D(l, u, e, n))
    L = list(itertools.product(*L))
    L = [tuple(zip(*t)) for t in L]
    return L
    
def implicitplot(p, lower=-1, upper=1, resolution=None, blocks=1, method='dct_rescale', vars_list = (), precision=None):
    import visvis
    if p.ndim not in [2, 3]:
        raise ValueError("The dimension of the ambiant space must be 2 or 3")
    if resolution is None:
        resolution = 2**12 if p.ndim == 2 else 2**8
    if not hasattr(lower, '__iter__'):
        lower = (lower,)
    if not hasattr(upper, '__iter__'):
        upper = (upper,)
    if not hasattr(resolution, '__iter__'):
        resolution = (resolution,)
    if not hasattr(blocks, '__iter__'):
        blocks = (blocks,)
    lower = np.append(np.array(lower), (lower[-1],)*(p.ndim - len(lower)))
    upper = np.append(np.array(upper), (upper[-1],)*(p.ndim - len(upper)))
    resolution = np.append(np.array(resolution), (resolution[-1],)*(p.ndim - len(resolution)))
    blocks = np.append(np.array(blocks), (blocks[-1],)*(p.ndim - len(blocks)))
    epsilon = 2.0*(upper - lower)/(resolution*blocks)
    boxes = split_nD(lower, upper, epsilon, blocks)
    for b in boxes:
        if method == 'horner':
            if p.dtype==np.float32 or p.dtype==np.float64:
                g = evalgrid_horner(p, chebyshev_nodes(resolution, b[0], b[1], dtype=p.dtype))
            else:
                if precision is None:
                    precision = 100
                import flint as fl
                prec_save = fl.ctx.prec
                fl.ctx.prec = precision
                ap = np.vectorize(to_arb)(p)
                g = evalgrid_horner(ap, chebyshev_nodes(resolution, b[0], b[1], dtype=object))
                g = g.astype('float64')
                fl.ctx.prec = prec_save
        elif method == 'taylorshift':
            t = poly2cheb_taylorshift(p, b[0], b[1])
            g = evalgrid_idct(t, resolution)
        elif method == 'dct':
            t = poly2cheb_dct(p.astype('float64'), b[0], b[1])
            g = evalgrid_idct(t, resolution)
        elif method == 'dct_rescale':
            if precision is None:
                precision = 2**9 if p.ndim == 2 else 2**7
            t = poly2cheb_dct_rescale(p.astype('float64'), b[0], b[1], precision)
            g = evalgrid_idct(t, resolution)
        else:
            raise ValueError("method is one of 'taylorshift', 'horner', 'dct', 'dct_rescale'")
        if g.min() > 0 or g.max() < 0:
            continue
        if p.ndim == 2:
            points = implicitmesh_chebgrid_2d(g, *b)
            visvis.plot(points[:,0], points[:,1], ls='+')
        elif p.ndim == 3:
            verts, faces = implicitmesh_chebgrid_3d(g, *b)
            surf = visvis.mesh(verts, faces)
            surf.faceColor = [c/255.0 for c in (149,208,252)]
    dummy_vars = ("x", "y", "z")
    vars_list += dummy_vars[len(vars_list):]
    visvis.xlabel(vars_list[0])
    visvis.ylabel(vars_list[1])
    if p.ndim == 3:
        visvis.zlabel(vars_list[2])
    visvis.use().Run()

