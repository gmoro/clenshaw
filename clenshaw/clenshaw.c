/****************************************************************************
        Copyright (C) 2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

#define _XOPEN_SOURCE 600
#pragma STDC FENV_ACCESS ON
#include "stdlib.h"
#include "stdio.h"
#include "time.h"
#include "fenv.h"
#include "string.h"
#include "math.h"
#include "stdint.h"
#include "stddef.h"
#include "clenshaw.h"

// simd parameters
#ifdef __AVX512CD__
#define Rbits 512
#elif __AVX__
#define Rbits 256
#elif __SSE__
#define Rbits 128
#else
#define Rbits 64
#endif
#define Rbytes (Rbits/8)
#define Nd (Rbytes/8)

// Chunk sizes
#define Latency  6

typedef float  vecs __attribute__ ((vector_size(Rbytes), aligned(Rbytes))) ;
typedef double vecd __attribute__ ((vector_size(Rbytes), aligned(Rbytes)));
typedef uint64_t vecul __attribute__ ((vector_size(Rbytes), aligned(Rbytes)));
typedef uint32_t vecui __attribute__ ((vector_size(Rbytes), aligned(Rbytes)));

// Tools
static uintptr_t next_aligned(uintptr_t addr) {
    uintptr_t mask = ~(uintptr_t)(Rbytes-1);
    return (((uintptr_t) addr + Rbytes - 1) & mask);
}

static ptrdiff_t min(ptrdiff_t a, ptrdiff_t b) {
    return (a<b)?a:b;
}

// Single precision horner
static void _vs_horner(vecs (*inout)[Latency], int m, const float* coefficients, int n)
{
    #pragma omp parallel 
    {
        //int round = fegetround();
        //fesetround(FE_UPWARD); // rounding mode must be set in each thread
        #pragma omp for
        for(int i=0; i < m; i++) {
            //const vecs (*v)[Latency] = (values + i);
            vecs r[Latency] = {{0}};
            for(int j=0; j<n; j++) {
                const float c = coefficients[n-1-j];
                #pragma GCC unroll 8
                for(int k=0; k<Latency; k++) {
                    #pragma clang fp contract(on)
                    r[k] = r[k]*inout[i][k] + c;
                }
            }
            memcpy((void*)(inout+i), r, Latency*Rbytes);
        }
        //fesetround(round);
    }
}

void s_horner(float* inout, int m,
              const float* coefficients, int n)
{
    uintptr_t sinout, sainout, einout;
    int sblock = Rbytes*Latency;
    //int round = fegetround();
    //fesetround(FE_UPWARD);
    vecs v[Latency] = {{0}};
    vecs w[Latency] = {{0}};
    sinout = (uintptr_t) inout;
    einout = sinout + m*sizeof(float);
    if(einout <= sinout + sblock) {
        memcpy((void*) &v, inout, einout - sinout);
        _vs_horner(&v, 1, coefficients, n);
        memcpy((void*) inout, &v, einout - sinout);
    } else {
        sainout = next_aligned(sinout);
        ptrdiff_t M = (einout-sainout)/(Rbytes*Latency);
        memcpy((void*) &v, inout, sblock);
        memcpy((void*) &w, (void*) einout - sblock, sblock);
        _vs_horner(&v, 1, coefficients, n);
        _vs_horner((vecs (*)[Latency]) sainout, M, coefficients, n);
        _vs_horner(&w, 1, coefficients, n);
        memcpy((void*) einout - sblock, &w, sblock);
        memcpy((void*) inout, &v, sblock);
    }
    //fesetround(round);
}

// Double precision horner
static void _vd_horner(vecd (*inout)[Latency], int m, const double* coefficients, int n)
{
    #pragma omp parallel 
    {
        //int round = fegetround();
        //fesetround(FE_UPWARD); // rounding mode must be set in each thread
        #pragma omp for
        for(int i=0; i < m; i++) {
            vecd r[Latency] = {{0}};
            for(int j=0; j<n; j++) {
                const double c = coefficients[n-1-j];
                #pragma GCC unroll 8
                for(int k=0; k<Latency; k++) {
                    #pragma clang fp contract(on)
                    r[k] = r[k]*inout[i][k] + c;
                }
            }
            memcpy((void*)(inout+i), r, Latency*Rbytes);
        }
        //fesetround(round);
    }
}

void d_horner(double* inout, int m,
              const double* coefficients, int n)
{
    uintptr_t sinout, sainout, einout;
    int sblock = Rbytes*Latency;
    //int round = fegetround();
    //fesetround(FE_UPWARD);
    vecd v[Latency] = {{0}};
    vecd w[Latency] = {{0}};
    sinout = (uintptr_t) inout;
    einout = sinout + m*sizeof(double);
    if(einout <= sinout + sblock) {
        memcpy((void*) &v, inout, einout - sinout);
        _vd_horner(&v, 1, coefficients, n);
        memcpy((void*) inout, &v, einout - sinout);
    } else {
        sainout = next_aligned(sinout);
        ptrdiff_t M = (einout-sainout)/(Rbytes*Latency);
        memcpy((void*) &v, inout, sblock);
        memcpy((void*) &w, (void*) einout - sblock, sblock);
        _vd_horner(&v, 1, coefficients, n);
        _vd_horner((vecd (*)[Latency]) sainout, M, coefficients, n);
        _vd_horner(&w, 1, coefficients, n);
        memcpy((void*) einout - sblock, &w, sblock);
        memcpy((void*) inout, &v, sblock);
    }
    //fesetround(round);
}

// Single precision balls horner
static void _vsb_horner(vecs (* restrict inout_centers)[Latency],
                 vecs (* restrict inout_radii)[Latency], int m,
                 const float* coefficients, int n)
{
    vecui abs_mask = {0};
    abs_mask += (uint32_t)-1 >> 1;
    float u = 0x1p-23;
    #pragma omp parallel 
    {
        int round = fegetround();
        fesetround(FE_UPWARD); // rounding mode must be set in each thread
        #pragma omp for
        for(int i=0; i < m; i++) {
            vecs rc[Latency] = {{0}};
            vecs rr[Latency] = {{0}};
            vecs vcr[Latency];
            vecs vrucr[Latency];
            for(int k = 0; k<Latency; k++) {
                vecs vca = (vecs) ((vecui) inout_centers[i][k] & abs_mask); //taking absolute value
                vcr[k] = inout_radii[i][k] + vca;
                vrucr[k] = inout_radii[i][k] + u*(vca+vcr[k]);
            }
            for(int j=0; j<n; j++) {
                const float c = coefficients[n-1-j];
                #pragma GCC unroll 8
                for(int k=0; k<Latency; k++) {
                    #pragma clang fp contract(on)
                    vecs rca = (vecs) ((vecui) rc[k] & abs_mask); //taking absolute value 
                    rr[k] = rr[k]*vcr[k] + rca;
                    rc[k] = rc[k]*inout_centers[i][k] + c;
                }
            }
            for(int k = 0; k<Latency; k++) {
                vecs rca = (vecs) ((vecui) rc[k] & abs_mask); //taking absolute value 
                inout_radii[i][k] = rr[k]*vrucr[k]+u*rca;
                inout_centers[i][k] = rc[k];
            }
        }
        fesetround(round);
    }
}

void sb_horner(float* restrict inout_centers, float* restrict inout_radii, int m,
              const float* coefficients, int n)
{
    uintptr_t sioc, saioc, eioc, sior, saior, eior;
    int sblock = Rbytes*Latency;
    vecs vc[Latency] = {{0}};
    vecs vr[Latency] = {{0}};
    vecs wc[Latency] = {{0}};
    vecs wr[Latency] = {{0}};
    sioc = (uintptr_t) inout_centers;
    eioc = sioc + m*sizeof(float);
    sior = (uintptr_t) inout_radii;
    eior = sior + m*sizeof(float);
    if( eioc <= sioc + sblock) {
        memcpy((void*) &vc, inout_centers, eioc-sioc);
        memcpy((void*) &vr, inout_radii, eior-sior);
        _vsb_horner(&vc, &vr, 1, coefficients, n);
        memcpy((void*) inout_centers, &vc, eioc-sioc);
        memcpy((void*) inout_radii, &vr, eior-sior);
    } else {
        saioc = next_aligned(sioc);
        saior = next_aligned(sior);
        ptrdiff_t M = min(eioc-saioc, eior-saior)/sblock;
        memcpy((void*) vc, (void*) sioc, sblock);
        memcpy((void*) vr, (void*) sior, sblock);
        memcpy((void*) wc, (void*) eioc - sblock, sblock);
        memcpy((void*) wr, (void*) eior - sblock, sblock);
        memmove((void*) saior, (void*) sior + saioc - sioc, M*sblock);
        _vsb_horner(&vc, &vr, 1, coefficients, n);
        _vsb_horner(&wc, &wr, 1, coefficients, n);
        _vsb_horner((vecs (*)[Latency]) saioc, (vecs (*)[Latency]) saior, M, coefficients, n);
        memmove((void*) sior + saioc - sioc, (void*) saior, M*sblock);
        memcpy((void*) sioc, &vc, sblock);
        memcpy((void*) sior, &vr, sblock);
        memcpy((void*) eioc - sblock, wc, sblock);
        memcpy((void*) eior - sblock, wr, sblock);
    }
}

// Double precision balls horner
static void _vdb_horner(vecd (* restrict inout_centers)[Latency],
                 vecd (* restrict inout_radii)[Latency], int m,
                 const double* coefficients, int n)
{
    vecul abs_mask = {0};
    abs_mask += (uint64_t)-1 >> 1;
    double u = 0x1p-52;
    #pragma omp parallel 
    {
        int round = fegetround();
        fesetround(FE_UPWARD); // rounding mode must be set in each thread
        #pragma omp for
        for(int i=0; i < m; i++) {
            vecd rc[Latency] = {{0}};
            vecd rr[Latency] = {{0}};
            vecd vcr[Latency];
            vecd vrucr[Latency];
            for(int k = 0; k<Latency; k++) {
                vecd vca = (vecd) ((vecul) inout_centers[i][k] & abs_mask); //taking absolute value
                vcr[k] = inout_radii[i][k] + vca;
                vrucr[k] = inout_radii[i][k] + u*(vca+vcr[k]);
            }
            for(int j=0; j<n; j++) {
                const double c = coefficients[n-1-j];
                #pragma GCC unroll 8
                for(int k=0; k<Latency; k++) {
                    #pragma clang fp contract(on)
                    vecd rca = (vecd) ((vecul) rc[k] & abs_mask); //taking absolute value 
                    rr[k] = rr[k]*vcr[k] + rca;
                    rc[k] = rc[k]*inout_centers[i][k] + c;
                }
            }
            for(int k = 0; k<Latency; k++) {
                vecd rca = (vecd) ((vecul) rc[k] & abs_mask); //taking absolute value 
                inout_radii[i][k] = rr[k]*vrucr[k]+u*rca;
                inout_centers[i][k] = rc[k];
            }
        }
        fesetround(round);
    }
}

void db_horner(double* restrict inout_centers, double* restrict inout_radii, int m,
              const double* coefficients, int n)
{
    uintptr_t sioc, saioc, eioc, sior, saior, eior;
    int sblock = Rbytes*Latency;
    vecd vc[Latency] = {{0}};
    vecd vr[Latency] = {{0}};
    vecd wc[Latency] = {{0}};
    vecd wr[Latency] = {{0}};
    sioc = (uintptr_t) inout_centers;
    eioc = sioc + m*sizeof(double);
    sior = (uintptr_t) inout_radii;
    eior = sior + m*sizeof(double);
    if( eioc <= sioc + sblock) {
        memcpy((void*) &vc, inout_centers, eioc-sioc);
        memcpy((void*) &vr, inout_radii, eior-sior);
        _vdb_horner(&vc, &vr, 1, coefficients, n);
        memcpy((void*) inout_centers, &vc, eioc-sioc);
        memcpy((void*) inout_radii, &vr, eior-sior);
    } else {
        saioc = next_aligned(sioc);
        saior = next_aligned(sior);
        ptrdiff_t M = min(eioc-saioc, eior-saior)/sblock;
        memcpy((void*) vc, (void*) sioc, sblock);
        memcpy((void*) vr, (void*) sior, sblock);
        memcpy((void*) wc, (void*) eioc - sblock, sblock);
        memcpy((void*) wr, (void*) eior - sblock, sblock);
        memmove((void*) saior, (void*) sior + saioc - sioc, M*sblock);
        _vdb_horner(&vc, &vr, 1, coefficients, n);
        _vdb_horner(&wc, &wr, 1, coefficients, n);
        _vdb_horner((vecd (*)[Latency]) saioc, (vecd (*)[Latency]) saior, M, coefficients, n);
        memmove((void*) sior + saioc - sioc, (void*) saior, M*sblock);
        memcpy((void*) sioc, &vc, sblock);
        memcpy((void*) sior, &vr, sblock);
        memcpy((void*) eioc - sblock, wc, sblock);
        memcpy((void*) eior - sblock, wr, sblock);
    }
}

// Single precision clenshaw
static void _vs_clenshaw(vecs (* restrict inout)[Latency], int m,
                         const float* coefficients, int n)
{
    #pragma omp parallel 
    {
        //int round = fegetround();
        //fesetround(FE_UPWARD); // rounding mode must be set in each thread
        #pragma omp for
        for(int i=0; i < m; i++) {
            vecs rca[Latency] = {{0}};
            vecs rcb[Latency] = {{0}};
            vecs vc2[Latency];
            for(int k = 0; k<Latency; k++) {
                vc2[k] = 2*inout[i][k];
            }
            // Clenshaw loop to n-2
            for(int j=n-1; j>0; j--) {
                const float c = coefficients[j];
                if(j&1) {
                    #pragma GCC unroll 8
                    for(int k=0; k<Latency; k++) {
                        #pragma clang fp contract(on)
                        rca[k] = (rcb[k]*vc2[k] + c) - rca[k];
                    }
                } else {
                    #pragma GCC unroll 8
                    for(int k=0; k<Latency; k++) {
                        #pragma clang fp contract(on)
                        rcb[k] = (rca[k]*vc2[k] + c) - rcb[k];
                    }
                }
            }
            for(int k = 0; k<Latency; k++) {
                inout[i][k] = (rca[k]*inout[i][k] + coefficients[0]) - rcb[k];
            }
        }
        //fesetround(round);
    }
}

void s_clenshaw(float* inout, int m,
                const float* coefficients, int n)
{
    uintptr_t sinout, sainout, einout;
    int sblock = Rbytes*Latency;
    vecs v[Latency] = {{0}};
    vecs w[Latency] = {{0}};
    sinout = (uintptr_t) inout;
    einout = sinout + m*sizeof(float);
    if(einout <= sinout + sblock) {
        memcpy((void*) &v, inout, einout - sinout);
        _vs_clenshaw(&v, 1, coefficients, n);
        memcpy((void*) inout, &v, einout - sinout);
    } else {
        sainout = next_aligned(sinout);
        ptrdiff_t M = (einout-sainout)/(Rbytes*Latency);
        memcpy((void*) &v, inout, sblock);
        memcpy((void*) &w, (void*) einout - sblock, sblock);
        _vs_clenshaw(&v, 1, coefficients, n);
        _vs_clenshaw((vecs (*)[Latency]) sainout, M, coefficients, n);
        _vs_clenshaw(&w, 1, coefficients, n);
        memcpy((void*) einout - sblock, &w, sblock);
        memcpy((void*) inout, &v, sblock);
    }
}

// Double precision clenshaw
static void _vd_clenshaw(vecd (* restrict inout)[Latency], int m,
                         const double* coefficients, int n)
{
    #pragma omp parallel 
    {
        //int round = fegetround();
        //fesetround(FE_UPWARD); // rounding mode must be set in each thread
        #pragma omp for
        for(int i=0; i < m; i++) {
            vecd rca[Latency] = {{0}};
            vecd rcb[Latency] = {{0}};
            vecd vc2[Latency];
            for(int k = 0; k<Latency; k++) {
                vc2[k] = 2*inout[i][k];
            }
            // Clenshaw loop to n-2
            for(int j=n-1; j>0; j--) {
                const double c = coefficients[j];
                if(j&1) {
                    #pragma GCC unroll 8
                    for(int k=0; k<Latency; k++) {
                        #pragma clang fp contract(on)
                        rca[k] = (rcb[k]*vc2[k] + c) - rca[k];
                    }
                } else {
                    #pragma GCC unroll 8
                    for(int k=0; k<Latency; k++) {
                        #pragma clang fp contract(on)
                        rcb[k] = (rca[k]*vc2[k] + c) - rcb[k];
                    }
                }
                //printf("%d, %d: %.20f %.20f\n", i, j, rca[0][0], rcb[0][0]);
            }
            for(int k = 0; k<Latency; k++) {
                inout[i][k] = (rca[k]*inout[i][k] + coefficients[0]) - rcb[k];
            }
        }
        //fesetround(round);
    }
}

void d_clenshaw(double* inout, int m,
              const double* coefficients, int n)
{
    uintptr_t sinout, sainout, einout;
    int sblock = Rbytes*Latency;
    vecd v[Latency] = {{0}};
    vecd w[Latency] = {{0}};
    sinout = (uintptr_t) inout;
    einout = sinout + m*sizeof(double);
    if(einout <= sinout + sblock) {
        memcpy((void*) &v, inout, einout - sinout);
        _vd_clenshaw(&v, 1, coefficients, n);
        memcpy((void*) inout, &v, einout - sinout);
    } else {
        sainout = next_aligned(sinout);
        ptrdiff_t M = (einout-sainout)/(Rbytes*Latency);
        memcpy((void*) &v, inout, sblock);
        memcpy((void*) &w, (void*) einout - sblock, sblock);
        _vd_clenshaw(&v, 1, coefficients, n);
        _vd_clenshaw((vecd (*)[Latency]) sainout, M, coefficients, n);
        _vd_clenshaw(&w, 1, coefficients, n);
        memcpy((void*) einout - sblock, &w, sblock);
        memcpy((void*) inout, &v, sblock);
    }
}


// Single precision balls clenshaw
static void _vsb_clenshaw(vecs (* restrict inout_centers)[Latency],
                          vecs (* restrict inout_radii)[Latency], int m,
                          const float* coefficients, int n)
{
    vecui abs_mask = {0};
    abs_mask += (uint32_t)-1 >> 1;
    float u = 0x1p-23;
    #pragma omp parallel 
    {
        int round = fegetround();
        fesetround(FE_UPWARD); // rounding mode must be set in each thread
        #pragma omp for
        for(int i=0; i < m; i++) {
            vecs rca[Latency] = {{0}};
            vecs rcb[Latency] = {{0}};
            vecs rr[Latency] = {{0}};
            vecs vc2[Latency];
            for(int k = 0; k<Latency; k++) {
                vc2[k] = 2*inout_centers[i][k];
            }
            // Clenshaw loop to n-2
            for(int j=n-1; j>0; j--) {
                const float c = coefficients[j];
                if(j&1) {
                    #pragma GCC unroll 8
                    for(int k=0; k<Latency; k++) {
                        #pragma clang fp contract(on)
                        rr[k] += (vecs) ((vecui) rca[k] & abs_mask); //taking absolute value 
                        rca[k] = (rcb[k]*vc2[k] + c) - rca[k];
                    }
                } else {
                    #pragma GCC unroll 8
                    for(int k=0; k<Latency; k++) {
                        #pragma clang fp contract(on)
                        rr[k] += (vecs) ((vecui) rcb[k] & abs_mask); //taking absolute value 
                        rcb[k] = (rca[k]*vc2[k] + c) - rcb[k];
                    }
                }
            }
            for(int k = 0; k<Latency; k++) {
                vecs c_a = (vecs) ((vecui) inout_centers[i][k] & abs_mask); //taking absolute value 
                inout_centers[i][k] = (rca[k]*inout_centers[i][k] + coefficients[0]) - rcb[k];
                // Compute the radius
                vecs u0_a = (vecs) ((vecui) inout_centers[i][k] & abs_mask); //taking absolute value 
                vecs u1_a = (vecs) ((vecui) rca[k] & abs_mask); //taking absolute value 
                vecs u2_a = (vecs) ((vecui) rcb[k] & abs_mask); //taking absolute value 
                inout_radii[i][k] = (rr[k] + u2_a)*(2*inout_radii[i][k] + u*(2*c_a + 3))
                                    + u1_a*(inout_radii[i][k] + u*(c_a+2))
                                    + u0_a*u*2;
            }
        }
        fesetround(round);
    }
}

void sb_clenshaw(float* restrict inout_centers, float* restrict inout_radii, int m,
                 const float* coefficients, int n)
{
    uintptr_t sioc, saioc, eioc, sior, saior, eior;
    int sblock = Rbytes*Latency;
    vecs vc[Latency] = {{0}};
    vecs vr[Latency] = {{0}};
    vecs wc[Latency] = {{0}};
    vecs wr[Latency] = {{0}};
    sioc = (uintptr_t) inout_centers;
    eioc = sioc + m*sizeof(float);
    sior = (uintptr_t) inout_radii;
    eior = sior + m*sizeof(float);
    if( eioc <= sioc + sblock) {
        memcpy((void*) &vc, inout_centers, eioc-sioc);
        memcpy((void*) &vr, inout_radii, eior-sior);
        _vsb_clenshaw(&vc, &vr, 1, coefficients, n);
        memcpy((void*) inout_centers, &vc, eioc-sioc);
        memcpy((void*) inout_radii, &vr, eior-sior);
    } else {
        saioc = next_aligned(sioc);
        saior = next_aligned(sior);
        ptrdiff_t M = min(eioc-saioc, eior-saior)/sblock;
        memcpy((void*) vc, (void*) sioc, sblock);
        memcpy((void*) vr, (void*) sior, sblock);
        memcpy((void*) wc, (void*) eioc - sblock, sblock);
        memcpy((void*) wr, (void*) eior - sblock, sblock);
        memmove((void*) saior, (void*) sior + saioc - sioc, M*sblock);
        _vsb_clenshaw(&vc, &vr, 1, coefficients, n);
        _vsb_clenshaw(&wc, &wr, 1, coefficients, n);
        _vsb_clenshaw((vecs (*)[Latency]) saioc, (vecs (*)[Latency]) saior, M, coefficients, n);
        memmove((void*) sior + saioc - sioc, (void*) saior, M*sblock);
        memcpy((void*) sioc, &vc, sblock);
        memcpy((void*) sior, &vr, sblock);
        memcpy((void*) eioc - sblock, wc, sblock);
        memcpy((void*) eior - sblock, wr, sblock);
    }
}


// Double precision balls clenshaw
static void _vdb_clenshaw(vecd (* restrict inout_centers)[Latency],
                          vecd (* restrict inout_radii)[Latency], int m,
                          const double* coefficients, int n)
{
    vecul abs_mask = {0};
    abs_mask += (uint64_t)-1 >> 1;
    double u = 0x1p-52;
    //TODO: change rounding mode in each omp thread
    #pragma omp parallel 
    {
        int round = fegetround();
        fesetround(FE_UPWARD); // rounding mode must be set in each thread
        #pragma omp for
        for(int i=0; i < m; i++) {
            vecd rca[Latency] = {{0}};
            vecd rcb[Latency] = {{0}};
            vecd rr[Latency] = {{0}};
            vecd vc2[Latency];
            for(int k = 0; k<Latency; k++) {
                vc2[k] = 2*inout_centers[i][k];
            }
            // Clenshaw loop to n-2
            for(int j=n-1; j>0; j--) {
                const double c = coefficients[j];
                if(j&1) {
                    #pragma GCC unroll 8
                    for(int k=0; k<Latency; k++) {
                        #pragma clang fp contract(on)
                        rr[k] += (vecd) ((vecul) rca[k] & abs_mask); //taking absolute value 
                        rca[k] = (rcb[k]*vc2[k] + c) - rca[k];
                    }
                } else {
                    #pragma GCC unroll 8
                    for(int k=0; k<Latency; k++) {
                        #pragma clang fp contract(on)
                        rr[k] += (vecd) ((vecul) rcb[k] & abs_mask); //taking absolute value 
                        rcb[k] = (rca[k]*vc2[k] + c) - rcb[k];
                    }
                }
                //printf("%d, %d: %.20f %.20f\n", i, j, rca[0][0], rcb[0][0]);
            }
            for(int k = 0; k<Latency; k++) {
                vecd c_a = (vecd) ((vecul) inout_centers[i][k] & abs_mask); //taking absolute value 
                inout_centers[i][k] = (rca[k]*inout_centers[i][k] + coefficients[0]) - rcb[k];
                // Compute the radius
                vecd u0_a = (vecd) ((vecul) inout_centers[i][k] & abs_mask); //taking absolute value 
                vecd u1_a = (vecd) ((vecul) rca[k] & abs_mask); //taking absolute value 
                vecd u2_a = (vecd) ((vecul) rcb[k] & abs_mask); //taking absolute value 
                inout_radii[i][k] = (rr[k] + u2_a)*(2*inout_radii[i][k] + u*(2*c_a + 3))
                                    + u1_a*(inout_radii[i][k] + u*(c_a+2))
                                    + u0_a*u*2;
            }
        }
        fesetround(round);
    }
}

void db_clenshaw(double* restrict inout_centers, double* restrict inout_radii, int m,
                 const double* coefficients, int n)
{
    uintptr_t sioc, saioc, eioc, sior, saior, eior;
    int sblock = Rbytes*Latency;
    vecd vc[Latency] = {{0}};
    vecd vr[Latency] = {{0}};
    vecd wc[Latency] = {{0}};
    vecd wr[Latency] = {{0}};
    sioc = (uintptr_t) inout_centers;
    eioc = sioc + m*sizeof(double);
    sior = (uintptr_t) inout_radii;
    eior = sior + m*sizeof(double);
    if( eioc <= sioc + sblock) {
        memcpy((void*) &vc, inout_centers, eioc-sioc);
        memcpy((void*) &vr, inout_radii, eior-sior);
        _vdb_clenshaw(&vc, &vr, 1, coefficients, n);
        memcpy((void*) inout_centers, &vc, eioc-sioc);
        memcpy((void*) inout_radii, &vr, eior-sior);
    } else {
        saioc = next_aligned(sioc);
        saior = next_aligned(sior);
        ptrdiff_t M = min(eioc-saioc, eior-saior)/sblock;
        memcpy((void*) vc, (void*) sioc, sblock);
        memcpy((void*) vr, (void*) sior, sblock);
        memcpy((void*) wc, (void*) eioc - sblock, sblock);
        memcpy((void*) wr, (void*) eior - sblock, sblock);
        memmove((void*) saior, (void*) sior + saioc - sioc, M*sblock);
        _vdb_clenshaw(&vc, &vr, 1, coefficients, n);
        _vdb_clenshaw(&wc, &wr, 1, coefficients, n);
        _vdb_clenshaw((vecd (*)[Latency]) saioc, (vecd (*)[Latency]) saior, M, coefficients, n);
        memmove((void*) sior + saioc - sioc, (void*) saior, M*sblock);
        memcpy((void*) sioc, &vc, sblock);
        memcpy((void*) sior, &vr, sblock);
        memcpy((void*) eioc - sblock, wc, sblock);
        memcpy((void*) eior - sblock, wr, sblock);
    }
}

// Double precision Chebyshev transform
// warning: lots of indices difficult to get right
static void _vd_chebyshev(vecd (* out)[2*Latency], int m,
                          const double* coefficients, int n, int offset)
{
    #pragma omp parallel 
    {
        //int round = fegetround();
        //fesetround(FE_UPWARD); // rounding mode must be set in each thread
        #pragma omp for
        for(int i=0; i < m; i++) {
            vecd beven[Latency] = {{0}};
            vecd bodd[Latency]  = {{0}};
            vecd reven[Latency] = {{0}};
            vecd rodd[Latency]  = {{0}};
            int start = offset + 2*i*Latency*Nd; // starting with degree at least start
            //if(start >= 1022) { // stop at subnormal numbers
            //    continue;
            //}
            double d = (double) start/2;
            vecd ind;
            for(int k=0; k<Nd; k++) {
                ind[k] = start + k*Latency;
            }
            beven[0][0] = ldexp(1, -start);
            if(start < n) {
                reven[0][0] = coefficients[start]*beven[0][0];
            }
            for(int j=1; j<n-start; j++) {
                const double c = coefficients[start+j];
                d += 0.5;
                if(j&1) {
                    #pragma GCC unroll 8
                    for(int k=0; k<Latency-1; k++) {
                        #pragma clang fp contract(on)
                        bodd[k] = (beven[k] + beven[k+1])/2;
                        rodd[k] += bodd[k]*c;
                    }
                    ind += Latency;
                    bodd[Latency-1]  = beven[Latency-1]*(d/ind);
                    rodd[Latency-1] += bodd[Latency-1]*c;
                } else {
                    ind -= (Latency-1);
                    beven[0][0] = bodd[0][0]*(d/(2*d-ind[0]));
                    #pragma GCC unroll 8
                    for(int k=0; k<Nd-1; k++) {
                        beven[0][k+1] = (bodd[0][k+1] + bodd[Latency-1][k])/2;
                    }
                    reven[0] += beven[0]*c;
                    #pragma GCC unroll 8
                    for(int k=0; k<Latency-1; k++) {
                        #pragma clang fp contract(on)
                        beven[k+1] = (bodd[k] + bodd[k+1])/2;
                        reven[k+1] += beven[k+1]*c;
                    }
                }
            }
            for(int j = 0; j<Latency; j++) {
                for(int k = 0; k<Nd; k++) {
                    // transposing back the result coefficients
                    *(((double*) (out + i)) + 2*(k*Latency+j))     = 2*reven[j][k];
                    *(((double*) (out + i)) + 2*(k*Latency+j) + 1) = 2*rodd[j][k];
                }
            }
        }
        //fesetround(round);
    }
    if((m>0) && (offset == 0)) {
        out[0][0][0] /= 2;
    }
}

void d_chebyshev(double* out, int m,
                 const double* coefficients, int n)
{
    uintptr_t sinout, sainout, einout;
    const int sblock = Rbytes*2*Latency;
    vecd v[2*Latency] = {{0}};
    vecd w[2*Latency] = {{0}};
    sinout = (uintptr_t) out;
    einout = sinout + m*sizeof(double);
    if(einout <= sinout + sblock) {
        memcpy((void*) &v, out, einout - sinout);
        _vd_chebyshev(&v, 1, coefficients, n, 0);
        memcpy((void*) out, &v, einout - sinout);
    } else {
        sainout = next_aligned(sinout);
        // the following assumes array aligned to double
        int offset1 = (sainout-sinout)/sizeof(double);
        int offset2 = (einout-sblock-sinout)/sizeof(double);
        ptrdiff_t M = (einout-sainout)/sblock;
        memcpy((void*) &v, out, sblock);
        memcpy((void*) &w, (void*) einout - sblock, sblock);
        _vd_chebyshev(&v, 1, coefficients, n, 0);
        _vd_chebyshev((vecd (*)[2*Latency]) sainout, M, coefficients, n, offset1);
        _vd_chebyshev(&w, 1, coefficients, n, offset2);
        memcpy((void*) einout - sblock, &w, sblock);
        memcpy((void*) out, &v, sblock);
    }
}

// Double precision Chebyshev transform with error bound
// use horner like algorithm for tighter error bound
// simpler but use more divisions
static void _vdb_chebyshev(vecd (* out)[2*Latency], int m,
                           double* radius,
                           const double* coefficients, int n, int offset)
{
    const int prec = 100; // should be less than 1022 when multiplied by 8, due to lower bound on binomial coef
    const int alpha = ceil(1.4*prec); // upper integer bound on (2*ln 2 * prec)
    vecul abs_mask = {0};
    abs_mask += (uint64_t)-1 >> 1;
    vecul exp_mask = {0};
    exp_mask += (uint64_t) 0x7FF0000000000000L;
    *radius = 0;
    int round = fegetround();
    fesetround(FE_UPWARD);
    #pragma omp parallel 
    {
        fesetround(FE_UPWARD); // rounding mode must be set in each thread
        double local_radius = 0;
        #pragma omp for schedule(dynamic)
        for(int i=0; i<m ; i++) {
            vecd num[2*Latency] = {{0}};
            vecd den[2*Latency] = {{0}};
            vecd coef[2*Latency] = {{0}};
            vecd res[2*Latency] = {{0}};
            vecd err[2*Latency] = {{0}};
            vecd err2[2*Latency] = {{0}};
            vecd ind[2*Latency] = {{0}};
            vecul cond_offset_res[2*Latency] = {{0}};
            vecul cond_offset_err[2*Latency] = {{0}};
            int64_t start = offset + 2*i*Latency*Nd; // starting with degree at least start
            int64_t mid = start <= alpha/2 ? start : start*start/alpha;
            int64_t startprev = start - 2*Latency*Nd;
            int64_t prev = startprev <= alpha/2 ? start : startprev*startprev/alpha + 2*Latency*Nd-1;
            if(prev > n-1) {
                for(int k=0; k < 2*Latency; k++) {
                    out[i][k] = (vecd) {0};
                }
                continue;
            }
            int parity = (n-offset) & 1;
            for(int k=0; k<2*Latency; k++) {
                for(int j=0; j<Nd; j++) {
                    ind[k][j] = 4*(start + k*Nd + j) - 4;
                    num[k][j] = ((double) n+parity+ k*Nd+j)*(n+parity+k*Nd+j-1);
                    den[k][j] = ((double) n+parity- start)*(n+parity+ start + 2*(k*Nd+j));
                    cond_offset_res[k][j] = start + k*Nd + j;
                    cond_offset_err[k][j] = start + k*Nd + j;
                }
            }
            ((double*) ind)[0] = 0;
            ((double*) ind)[1] = 0;
            if (parity == 1 && n >= offset) {
                coef[0][0] = coefficients[n-1];
                res[0][0]  = coefficients[n-1];
            } else if (n>=2 && n >= offset) {
                ((double*)coef)[0] = coefficients[n-2];
                ((double*)coef)[1] = coefficients[n-1];
                ((double*)res)[0]  = coefficients[n-2];
                ((double*)res)[1]  = coefficients[n-1];
            }
            //printf("first : %d -> %lu -> %d: %.5f %.5f %.5f %.5f \n", n, cond_offset_res[0][0], start, num[0][0]/den[0][0], res[0][0], err[0][0], err2[0][0]);
            for(int j=n-4+parity; j >= start; j -= 2) {
                memmove((double *) coef + 2, coef, 2*Latency*Rbytes - 2*sizeof(double));
                memmove((double *) coef, coefficients + j , 2*sizeof(double));
                memmove((double *) num + 2,  num,  2*Latency*Rbytes - 2*sizeof(double));
                memmove((double *) den + 2,  den,  2*Latency*Rbytes - 2*sizeof(double));
                ((double*)num)[0] -= 4*j + 10;
                ((double*)den)[0] -= 4*j + 12;
                ((double*)num)[1] -= 4*j + 14;
                ((double*)den)[1] -= 4*j + 16;
                if( j >= mid ) {
                    #pragma GCC unroll 16
                    for(int k=0; k<2*Latency; k++) {
                        #pragma clang fp contract(on)
                        den[k] -= ind[k]; 
                        // div[0][0] = (j+2)*(j+1)/ ( (j+2)^2 - start^2 )
                        vecd div = (num[k]/den[k]);
                        vecd a_err = (vecd) ((vecul) res[k] & abs_mask); //taking absolute value 
                        res[k] = div*res[k] + coef[k];
                        a_err  = 2*div*a_err + (vecd) ((vecul) res[k] & abs_mask); //taking absolute value 
                        err[k] = div*err[k] + a_err;
                    }
                } else {
                    #pragma GCC unroll 16
                    for(int k=0; k<2*Latency; k++) {
                        #pragma clang fp contract(on)
                        den[k] -= ind[k]; 
                        // div[0][0] = (j+2)*(j+1)/ ( (j+2)^2 - start^2 )
                        vecd div = (num[k]/den[k]);
                        res[k] = div*res[k];
                        err[k] = div*err[k];
                        // keep res and err within double range
                        vecul exp_res = ((vecul) res[k] & exp_mask) >> 52; //taking exponent part
                        vecul excess_res = ((exp_res <= cond_offset_res[k]) & (exp_res > 0) & (exp_res-1))
                                          |((exp_res >  cond_offset_res[k]) & cond_offset_res[k]); 
                        cond_offset_res[k] -= ((res[k]==0) & cond_offset_res[k]) | excess_res;
                        vecul exp_err = ((vecul) err[k] & exp_mask) >> 52; //taking exponent part
                        vecul excess_err = ((exp_err <= cond_offset_err[k]) & (exp_err > 0) & (exp_err-1))
                                          |((exp_err >  cond_offset_err[k]) & cond_offset_err[k]); 
                        cond_offset_err[k] -= ((err[k]==0) & cond_offset_err[k]) | excess_err;
                        res[k] = (vecd)((vecul) res[k] - (excess_res << 52));
                        err[k] = (vecd)((vecul) err[k] - (excess_err << 52));
                        //if(k==0) {
                            //printf("%lu, %lu, %lu, %lu, %lu\n", exp_res[0], exp_err[0], ((vecul) err[k])[0], excess_res[0], excess_err[0]);
                        //}
                        err2[k] += (vecd) ((vecul) res[k] & abs_mask); //absolute value, will be multiplied by
                                                                       //2u*2^-cond_res*tail of binomial*2
                    }
                //if(err[2*Latency-1][0] > 1) {
                //    printf("%d -> %lu -> %d: %.5f %.5f %.5f %.5f \n", j, cond_offset_res[0][0], start, num[0][0]/den[0][0], res[0][0], err[0][0], err2[0][0]);
                //}
                }
            }

            for(int k=0; k<2*Latency; k++) {
                for(int j=0; j<Nd; j++) {
                    out[i][k][j] = ldexp(res[k][j], 1 - (int64_t) cond_offset_res[k][j]); //coeff doubled, for z^k and 1/z^k
                    local_radius += ldexp(err[k][j], -52 + 1 - (int64_t) cond_offset_res[k][j] - (int64_t) cond_offset_err[k][j]);
                    local_radius += ldexp(err2[k][j], - 52 - prec + 1 - (int64_t) cond_offset_res[k][j]);
                }
            }
            //printf("%.5f\n", local_radius);
        }
        #pragma omp critical
        *radius += local_radius;
    }
    int64_t last = offset+2*(m-1)*Latency*Nd;
    int64_t prev = last <= alpha/2 ? offset : last*last/alpha + 2*Latency*Nd - 1;
    prev = prev > n-1 ? n : prev;
    //printf("before: %.5f\n", *radius);
    for(int j=offset; j<prev; j++) {
        *radius += ldexp(fabs(coefficients[j]), 1-prec);
    }
    //printf("after:  %.5f\n", *radius);
    if((m > 0) && (offset == 0)) {
        out[0][0][0] /= 2;
    }
    fesetround(round);
}

void db_chebyshev(double* out, int m,
                  double* radius,
                  const double* coefficients, int n)
{
    uintptr_t sinout, sainout, einout;
    const int sblock = Rbytes*2*Latency;
    vecd v[2*Latency] = {{0}};
    vecd w[2*Latency] = {{0}};
    sinout = (uintptr_t) out;
    einout = sinout + m*sizeof(double);
    if(einout <= sinout + sblock) {
        memcpy((void*) &v, out, einout - sinout);
        _vdb_chebyshev(&v, 1, radius, coefficients, n, 0);
        memcpy((void*) out, &v, einout - sinout);
    } else {
        sainout = next_aligned(sinout);
        int offset1 = (sainout-sinout)/sizeof(double);
        int offset2 = (einout-sblock-sinout)/sizeof(double);
        ptrdiff_t M = (einout-sainout)/sblock;
        memcpy((void*) &v, out, sblock);
        memcpy((void*) &w, (void*) einout - sblock, sblock);
        double r1, r2, r3;
        _vdb_chebyshev(&v, 1, &r1, coefficients, n, 0);
        _vdb_chebyshev((vecd (*)[2*Latency]) sainout, M, &r2, coefficients, n, offset1);
        _vdb_chebyshev(&w, 1, &r3, coefficients, n, offset2);
        *radius = r1 + r2 + r3;
        memcpy((void*) einout - sblock, &w, sblock);
        memcpy((void*) out, &v, sblock);
    }
    // bound error on the tail using binary entropy function
    // sum for i from 0 to k of binomial(n, k) <=  2^(n*(1-2/ln(2)(1/2-k/n)^2))
    // sum for i from 0 to k of binomial(p, p/2 - m/2) <=  2^p * exp^(-m^2/(2p))
    int round = fegetround();
    fesetround(FE_UPWARD);
    double rtail = 0;
    for(int p=m; p<n; p++) {
        rtail += ldexp(fabs(coefficients[p]), -(5*(int64_t) m*m/(7*p))); // 5/7 <= 1/2ln2
    }
    //printf("rtail: %.5f\n", rtail);

    *radius += 2*rtail;
    fesetround(round);
}

