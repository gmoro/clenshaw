#*****************************************************************************
#       Copyright (C) 2019 Guillaume Moroz <guillaume.moroz@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from clenshaw._interface import horner, clenshaw, chebyshev
import numpy as np
import fractions as fr


#def arb_to_fraction(a):
#    m, e = a.mid().man_exp()
#    f = fr.Fraction(int(m), 2**-int(e)) if e<=0 else int(m)*2**int(e)
#    return f

pi_prec = np.longdouble("3.14159265358979323846264338327950288419716939937510")

def chebyshev_nodes(N, lower = -1, upper = 1, dtype='float64'):
    if not hasattr(N, "__iter__"):
        N = [N] 
    if not hasattr(lower, '__iter__'):
        lower = (lower,)*len(N)
    else:
        lower = lower + (lower[-1],)*(len(N) - len(lower))
    if not hasattr(upper, '__iter__'):
        upper = (upper,)*len(N)
    else:
        upper = upper + (upper[-1],)*(len(N) - len(upper))
    if dtype == object:
        # use root of unity
        import flint as fl
        res = tuple(np.array([ fl.arb.cos_pi_fmpq(fl.fmpq(1+2*int(x),2*int(n))) for x in range(n) ]) for n in N)
        lower = tuple(to_arb(l) for l in lower)
        upper = tuple(to_arb(u) for u in upper)
    else:
        res = tuple(np.cos(pi_prec*(0.5+np.arange(n, dtype=np.longdouble))/n).astype(dtype) for n in N)
    return tuple((l+u)/2 + (u-l)/2*N for l,u,N in zip(lower, upper, res))


def uniform_nodes(N, dtype='float64'):
    if hasattr(N, "__iter__"):
        return tuple(np.arange(n, dtype=dtype)/(n-1) for n in N)
    else:
        return np.arange(N, dtype=dtype)/(N-1)


def uniform_split(k, dtype='float64'):
    if np.dtype(dtype) == np.float32 and k >= 22:
        raise ValueError("k should be less than 22 for type float32")
    elif np.dtype(dtype) == np.float64 and k >= 51:
        raise ValueError("k should be less than 51 for type float64")
    elif np.dtype(dtype) != np.float32 and np.dtype != np.float64:
        raise ValueError("dtype not supported")
    N = 2**k
    nodes = (2*np.arange(N) - (N-1))/N
    centers = np.array(nodes, dtype=dtype)
    radii   = np.array([1/N]*N, dtype=dtype)
    return centers, radii

def horner_generic(poly, values):
    n = poly.shape[-1]
    new_shape = poly.shape[:-1] + values.shape[-1:]
    res = np.broadcast_to(poly[...,n-1,None], new_shape)
    res = np.require(res, requirements=['C', 'A', 'W'])
    for i in range(n-1):
        res *= values
        res += poly[...,n-2-i, None]
    return res


def evalgrid_horner(poly, X):
    p = poly
    n = p.ndim
    #offset = 0
    #if(X[-1].size > poly.shape[-1]):
    #    offset = 1
    for i in range(n):
        # transpose smallest dimensions
        #if(X[-1].size > poly.shape[-1]):
        #    p = np.moveaxis(p, -1, 0)
        x = np.array(X[-1-i])
        p = np.ascontiguousarray(p)
        V = np.require(np.broadcast_to(x, p.shape[:-1] + (x.size,)),
                        requirements=['C', 'A', 'W'])
        if poly.dtype == np.float32 or poly.dtype == np.float64:
            p = horner(p, V)
        else:
            p = horner_generic(p, V)
        p = np.moveaxis(p, -1, 0)
        #if(X[-1].size <= poly.shape[-1]):
        #    p = np.moveaxis(p, -1, 0)
    return p


def evalgrid_clenshaw(poly, X):
    p = poly
    for i in range(p.ndim):
        p = np.ascontiguousarray(p)
        x = np.array(X[-1-i])
        V = np.require(np.broadcast_to(x, p.shape[:-1] + (x.size,)),
                        requirements=['C', 'A', 'W'])
        p = clenshaw(p, V)
        p = np.moveaxis(p, -1, 0)
    return p


def evalgrid_idct(poly, N):
    import scipy.fftpack as fp
    if not hasattr(N, "__iter__"):
        N = (N,)*poly.ndim
    p = poly.copy()
    if all(n>=s for n,s in zip(N, poly.shape)):
        for i in range(p.ndim):
            p[(slice(None),)*i + (0,)] *= 2
        # Bug in fftpack : not accepting array for shape
        V = fp.idctn(p, shape=tuple(N))/2**p.ndim
        return V
    for i in range(p.ndim):
        p = np.ascontiguousarray(p)
        if N[-1-i] < poly.shape[-1-i]:
            x = chebyshev_nodes(N[-1-i], dtype=p.dtype)
            V = np.require(np.broadcast_to(x, p.shape[:-1] + (x[0].size,)),
                            requirements=['C', 'A', 'W'])
            p = clenshaw(p, V)
        else:
            p[...,0] *= 2
            p = fp.idct(p, n=N[-1-i])/2
        p = np.moveaxis(p, -1, 0)
    return p



def to_arb(x):
    import flint as fl
    if type(x) is fr.Fraction:
        return fl.arb(fl.fmpq(x.numerator, x.denominator))
    if isinstance(x, np.integer):
        return fl.arb(int(x))
    if isinstance(x, np.floating):
        return fl.arb(float(x))
    return fl.arb(x)

#def poly2cheb_truncated(poly, lower=-1, upper=1, Ntruncate = None):
#    try:
#        p = np.vectorize(lambda x: fl.arb(fl.fmpq(x.numerator, x.denominator)))(poly)
#    except:
#        p = np.vectorize(fr.Fraction)(poly)
#        p = np.vectorize(lambda x: fl.arb(fl.fmpq(x.numerator, x.denominator)))(p)
#    if not hasattr(lower, '__iter__'):
#        lower = (lower,)*p.ndim
#    else:
#        lower = lower + (lower[-1],)*(p.ndim - len(lower))
#    if not hasattr(upper, '__iter__'):
#        upper = (upper,)*p.ndim
#    else:
#        upper = upper + (upper[-1],)*(p.ndim - len(upper))
#    a, b = [], []
#    for l, u in zip(lower, upper):
#        l, u = fr.Fraction(l), fr.Fraction(u)
#        l = fl.arb(fl.fmpq(l.numerator, l.denominator))
#        u = fl.arb(fl.fmpq(u.numerator, u.denominator))
#        a.append((l+u)/2)
#        b.append((u-l)/2)
#    for j in reversed(range(p.ndim)):
#        n = p.shape[-1]
#        res = np.zeros_like(p)
#        for i in range(0,N,n):
#            res.flat[i:i+d+1] =
#        p = np.moveaxis(res, -1, 0)
#    res = np.vectorize(int, otypes=[object])(p)/int(D)



# Following Pan algorithm, Pcma98 section 5
def poly2cheb_taylorshift(poly, lower=-1, upper=1, dtype='float64'):
    import flint as fl
    try:
        pn = np.vectorize(lambda x: fl.fmpz(x.numerator))(poly)
        pd = np.vectorize(lambda x: fl.fmpz(x.denominator))(poly)
    except:
        p = np.vectorize(fr.Fraction)(poly)
        pn = np.vectorize(lambda x: fl.fmpz(x.numerator))(p)
        pd = np.vectorize(lambda x: fl.fmpz(x.denominator))(p)
    N = pn.size
    D = fl.fmpz(1)
    for e in pd[pd!=1]:
        D = abs(D*(e//D.gcd(e)))
    p = pn*(D//pd)
    if not hasattr(lower, '__iter__'):
        lower = (lower,)*p.ndim
    else:
        lower = tuple(lower) + (lower[-1],)*(p.ndim - len(lower))
    if not hasattr(upper, '__iter__'):
        upper = (upper,)*p.ndim
    else:
        upper = tuple(upper) + (upper[-1],)*(p.ndim - len(upper))
    an, bn, den = [], [], []
    for l,u in zip(lower, upper):
        a = fr.Fraction(l+u)/2
        b = fr.Fraction(u-l)/2
        lcm = int(np.lcm(a.denominator, b.denominator))
        an.append(fl.fmpz(int((a*lcm).numerator)))
        bn.append(fl.fmpz(int((b*lcm).numerator)))
        den.append(fl.fmpz(lcm))
    for j in reversed(range(p.ndim)):
        n = p.shape[-1]
        D *= 2**(2*n)
        D *= den[j]**n
        res = np.zeros_like(p)
        for i in range(0,N,n):
            P = fl.fmpz_poly(list(p.flat[i:i+n]))
            d = P.degree()
            P = fl.fmpz_poly(list(P)[::-1])
            P = P(fl.fmpz_poly([0, den[j]]))
            P = fl.fmpz_poly([0]*(d-P.degree()) + list(P)[::-1])
            P = P(fl.fmpz_poly([an[j], bn[j]]))
            T1 = P(fl.fmpz_poly([-1,2]))
            R1 = fl.fmpz_poly(list(T1)[::-1])
            T2 = R1(fl.fmpz_poly([1,-1]))
            C  = T2(fl.fmpz_poly([0,0,1]))
            T3 = C(fl.fmpz_poly([-1,2]))
            R2 = fl.fmpz_poly([0]*(2*d-T3.degree()) + list(T3)[::-1])
            T4 = R2(fl.fmpz_poly([1,1]))
            S = T4//fl.fmpz_poly([0,]*d + [1])
            S *= 2*2**(2*n-2*d)*den[j]**(n-d)
            S -= S(0)//2
            res.flat[i:i+d+1] = S.coeffs()
        p = np.moveaxis(res, -1, 0)
    res = np.vectorize(lambda x,y: fr.Fraction(int(x), int(y)), otypes=[object])(p, D)
    return res.astype(dtype)

def poly2cheb_dct(poly, lower = -1, upper = 1):
    import scipy.fftpack as fp
    dtype = poly.dtype
    X = chebyshev_nodes(poly.shape, lower, upper, dtype)
    V = evalgrid_horner(poly, X)
    V = V if V.dtype == np.float32 else V.astype(np.float64)
    C = fp.dctn(V)/np.prod(V.shape)
    for i in range(C.ndim):
        C[(slice(None),)*i + (0,)] /= 2
    return C

def poly2cheb_dct_rescale(poly, lower = -1, upper = 1, precision = 100):
    import scipy.fftpack as fp
    dtype = poly.dtype
    shape = np.maximum((precision,)*poly.ndim, poly.shape)
    X = chebyshev_nodes(shape, lower, upper, dtype)
    scale = 1+evalgrid_horner(np.abs(poly), [np.abs(x) for x in X])
    #m = np.ogrid[tuple(slice(1,d) for d in poly.shape)]
    #s = [(slice(None),)*i + (slice(1,None),) for i in range(poly.ndim)]
    #scale = 1+np.sqrt(sum(evalgrid_horner(np.abs(poly[s[i]])*m[i], X)**2 for i in range(poly.ndim)))
    V = evalgrid_horner(poly, X)/scale
    V = V if V.dtype == np.float32 else V.astype(np.float64)
    C = fp.dctn(V)/np.prod(V.shape)
    for i in range(C.ndim):
        C[(slice(None),)*i + (0,)] /= 2
    return C

# Following Pan algorithm, Pcma98 section 5
def shift(poly, lower=-1, upper=1, dtype='float64'):
    import flint as fl
    try:
        pn = np.vectorize(lambda x: fl.fmpz(x.numerator))(poly)
        pd = np.vectorize(lambda x: fl.fmpz(x.denominator))(poly)
    except:
        p = np.vectorize(fr.Fraction)(poly)
        pn = np.vectorize(lambda x: fl.fmpz(x.numerator))(p)
        pd = np.vectorize(lambda x: fl.fmpz(x.denominator))(p)
    N = pn.size
    D = fl.fmpz(1)
    for e in pd[pd!=1]:
        D = abs(D*(e//D.gcd(e)))
    p = pn*(D//pd)
    if not hasattr(lower, '__iter__'):
        lower = (lower,)*p.ndim
    else:
        lower = lower + (lower[-1],)*(p.ndim - len(lower))
    if not hasattr(upper, '__iter__'):
        upper = (upper,)*p.ndim
    else:
        upper = upper + (upper[-1],)*(p.ndim - len(upper))
    an, bn, den = [], [], []
    for l,u in zip(lower, upper):
        a = fr.Fraction(l+u)/2
        b = fr.Fraction(u-l)/2
        lcm = int(np.lcm(a.denominator, b.denominator))
        an.append(fl.fmpz((a*lcm).numerator))
        bn.append(fl.fmpz((b*lcm).numerator))
        den.append(fl.fmpz(lcm))
    for j in reversed(range(p.ndim)):
        n = p.shape[-1]
        D *= den[j]**n
        res = np.zeros_like(p)
        for i in range(0,N,n):
            P = fl.fmpz_poly(list(p.flat[i:i+n]))
            d = P.degree()
            P = fl.fmpz_poly(list(P)[::-1])
            P = P(fl.fmpz_poly([0, den[j]]))
            P = fl.fmpz_poly([0]*(d-P.degree()) + list(P)[::-1])
            P = P(fl.fmpz_poly([an[j], bn[j]]))
            P *= den[j]**(n-d)
            res.flat[i:i+d+1] = P.coeffs()
        p = np.moveaxis(res, -1, 0)
    res = np.vectorize(lambda x,y: fr.Fraction(int(x), int(y)), otypes=[object])(p, D)
    return res.astype(dtype)

def poly2cheb_truncated(poly, lower=-1, upper=1, shape = None):
    p = poly
    if (lower, upper) != (-1, 1):
        p = shift(p, lower, upper)
    p = p.astype(np.float64)
    if shape is None:
        shape = p.shape
    else:
        shape = np.minimum(shape, p.shape)
    for i in reversed(range(p.ndim)):
        p = np.ascontiguousarray(p)
        C = np.zeros(p.shape[:-1] + (shape[i],), dtype=p.dtype)
        p = chebyshev(p, C)
        p = np.moveaxis(p, -1, 0)
    return p
