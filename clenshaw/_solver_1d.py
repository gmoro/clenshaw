#*****************************************************************************
#       Copyright (C) 2019 Guillaume Moroz <guillaume.moroz@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

from clenshaw._interface import horner, clenshaw, chebyshev
from clenshaw._eval_nd import to_arb
import flint as fl
import numpy as np

def split(centers, radii):
    u = 2**-52 if centers.dtype == np.float64 else 2**-23
    new_radii = radii.repeat(2)/2
    new_radii[0::2] = -new_radii[0::2]
    new_centers = centers.repeat(2) + new_radii
    is_exact = ((new_radii == new_centers - centers.repeat(2)) &
                (centers.repeat(2) == new_centers - new_radii))
    new_radii[0::2] = -new_radii[0::2] # back to absolute value
    new_radii[~is_exact] += u*(np.abs(new_centers[~is_exact]) + new_radii[~is_exact])
    return new_centers, new_radii

def split_exact(centers, radii):
    u = 2**-52 if centers.dtype == np.float64 else 2**-23
    new_radii = radii.repeat(2)/2
    new_centers = centers.repeat(2)
    new_centers[0::2] -= new_radii[0::2]
    new_centers[1::2] += new_radii[0::2]
    return new_centers, new_radii

def add_error(A, B):
    S = A + B
    is_exact = (A==S-B) & (B==S-A)
    u = 2**-52 if A.dtype == np.float64 else 2**-23
    E = np.zeros_like(S)
    E[~is_exact] = u*(np.abs(A[~is_exact]) + np.abs(B[~is_exact]))
    return S, E

def union(balls, start, last):
    lower, l_errors = add_error(balls[0,start], -balls[1,start])
    upper, u_errors = add_error(balls[0,last], balls[1,last])
    centers, c_errors = add_error((lower-l_errors)/2.0, (upper+u_errors)/2.0)
    radii, r_errors = add_error((upper+u_errors)/2.0, -(lower-l_errors)/2.0)
    return (centers, c_errors + r_errors + radii)

def solve(f, df, err_f = 0, err_df = 0, n = None, epsilon = None, k = None, verbose = False):
    if epsilon is None:
        epsilon = 2**-51 if f.dtype==np.float64 else 2**-22
    if k is None:
        k = (len(f)-1).bit_length()
    N = 2**k
    nodes = (2*np.arange(N, dtype=np.float64) - (N-1))/N
    centers = np.array([-1]+list(nodes)+[1], dtype=f.dtype)
    radii   = np.array([0]+[1.0/N]*N+[0], dtype=f.dtype)
    partition_centers = np.empty(0, dtype=f.dtype)
    partition_radii = np.empty(0, dtype=f.dtype)
    partition_labels = np.empty(0, dtype=f.dtype)
    #centers_monotoneus = []
    #radii_monotoneus = []
    #centers_nonzero = []
    #radii_nonzero = []
    #sign_nonzero = []
    #centers_unknowns = []
    #radii_unknowns = []
    d = 0
    prev = 0
    count = 0
    while(centers.size > 0 and (n is None or d < n)):
        d += 1
        # check if f is non zero
        count += centers.size
        fc, fr   = clenshaw(f, centers.copy(), radii.copy())
        is_nonzero = np.abs(fc) > fr + err_f
        new = np.flatnonzero(is_nonzero).size
        partition_centers.resize(prev+new, refcheck=False) # to allow profiling
        partition_centers[prev:prev+new] = centers[is_nonzero]
        partition_radii.resize(prev+new, refcheck=False)
        partition_radii[prev:prev+new] = radii[is_nonzero]
        partition_labels.resize(prev+new, refcheck=False)
        partition_labels[prev:prev+new] = np.sign(fc[is_nonzero])
        prev += new
        #centers_nonzero += list(centers[is_nonzero])
        #radii_nonzero += list(radii[is_nonzero])
        #sign_nonzero += list(np.sign(fc[is_nonzero]))
        centers = centers[~is_nonzero]
        radii = radii[~is_nonzero]

        # check if f is monotoneus
        count += centers.size
        dfc, dfr = clenshaw(df, centers.copy(), radii.copy())
        is_monotoneus = np.abs(dfc) > dfr + err_df
        new = np.flatnonzero(is_monotoneus).size
        partition_centers.resize(prev+new, refcheck=False)
        partition_centers[prev:prev+new] = centers[is_monotoneus]
        partition_radii.resize(prev+new, refcheck=False)
        partition_radii[prev:prev+new] = radii[is_monotoneus]
        partition_labels.resize(prev+new, refcheck=False)
        partition_labels[prev:prev+new] = 0
        prev += new
        #centers_monotoneus += list(centers[is_monotoneus])
        #radii_monotoneus += list(radii[is_monotoneus])
        centers = centers[~is_monotoneus]
        radii = radii[~is_monotoneus]

        # add radii zero not handled to unknowns
        is_small = radii <= epsilon
        new = np.flatnonzero(is_small).size
        partition_centers.resize(prev+new, refcheck=False)
        partition_centers[prev:prev+new] = centers[is_small]
        partition_radii.resize(prev+new, refcheck=False)
        partition_radii[prev:prev+new] = radii[is_small]
        partition_labels.resize(prev+new, refcheck=False)
        partition_labels[prev:prev+new] = 2
        prev += new
        #centers_unknowns += list(centers[is_small])
        #radii_unknowns   += list(radii[is_small])
        centers = centers[~is_small]
        radii = radii[~is_small]

        # split remaining balls
        if(d+k<=20):
            centers, radii = split_exact(centers, radii)
        else:
            centers, radii = split(centers, radii)
        #TODO: for d+k >= 52 or 23, use split that check change

    # add remaining balls to unknowns
    new = centers.size
    partition_centers.resize(prev+new, refcheck=False)
    partition_centers[prev:prev+new] = centers
    partition_radii.resize(prev+new, refcheck=False)
    partition_radii[prev:prev+new] = radii
    partition_labels.resize(prev+new, refcheck=False)
    partition_labels[prev:prev+new] = 2
    if verbose:
        print("Number of evaluations: ", count)
    #centers_unknowns += list(centers)
    #radii_unknowns   += list(radii)

    # sort the balls
    #Nmonotoneus, Nunknowns = len(centers_monotoneus), len(centers_unknowns)
    partition = np.stack([partition_centers, partition_radii, partition_labels])
    #partition = np.array([centers_nonzero + centers_monotoneus + centers_unknowns,
    #                      radii_nonzero   + radii_monotoneus   + radii_unknowns,
    #                      sign_nonzero    + [0]*Nmonotoneus    + [2]*Nunknowns],
    #                      dtype=centers.dtype)
    sorted_centers = partition[0].argsort()
    partition = partition[:, sorted_centers]

    # extract the solutions
    positive = np.concatenate([[True], partition[2] == 1,  [True]])
    negative = np.concatenate([[True], partition[2] == -1, [True]])
    known_sign = positive | negative
    unknown_sign = ~known_sign
    ## indices at the beginning of a zero interval
    start = unknown_sign[1:-1] & known_sign[:-2]
    start = np.flatnonzero(start)
    ## indices at the end of a zero interval
    last   = unknown_sign[1:-1] & known_sign[2:]
    last = np.flatnonzero(last)
    end = last + 1
    ## indices of unknow intervals
    unknown_pieces = np.flatnonzero(partition[2] == 2)
    unknown_indices = np.searchsorted(end, unknown_pieces)
    if start.size > 0 and start[0] == 0:
        unknown_indices = np.insert(unknown_indices, 0, 0)
    if end.size > 0 and end[-1] == partition.shape[1]:
        unknown_indices = np.append(unknown_indices, end.size-1)
    unknown_indices = np.unique(unknown_indices)
    unknown_mask = np.zeros(start.shape, dtype=bool)
    unknown_mask[unknown_indices] = True
    ## assign balls to solutions and unkonwns
    unknown_balls = union(partition, start[unknown_mask], last[unknown_mask])
    unknowns = np.stack(unknown_balls, axis=-1)
    ## indices of solution intervals
    start = start[~unknown_mask]
    last = last[~unknown_mask]
    end = last + 1
    solution_mask = partition[2][start-1] != partition[2][end]
    solution_balls = union(partition, start[solution_mask], last[solution_mask])
    solutions = np.stack(solution_balls, axis=-1)
    return solutions, unknowns

def chebder(p):
    dp = 2*p[1:]*np.arange(1,p.size)
    N = dp.size
    for i in range((N-1)//2):
        dp[N-3 - 2*i] += dp[N-1 - 2*i]
    for i in range((N-2)//2):
        dp[N-4 - 2*i] += dp[N-2 - 2*i]
    if dp.size > 0:
        dp[0]/=2
    return dp

def solve_polynomial_chebyshev(poly, n=None, verbose = False):
    prec_save = fl.ctx.prec
    fl.ctx.prec = 53
    if type(poly) == np.ndarray and poly.dtype == np.float64:
        Vn = np.vectorize(fl.arb)(poly)
    else:
        Va = np.array([to_arb(e) for e in poly])
        # Reduce coefficient size for ieee floating point arithmetic
        man, exp = np.abs(Va).max().upper().man_exp()
        M = 2**(man.bit_length() + exp - 1)
        Vn = Va/M
    # Compute derivative
    dVn = chebder(Vn)
    # Error bounds on the coefficients
    err_f = float(sum(e.rad() for e in Vn).upper())
    err_df = float(sum(e.rad() for e in dVn).upper())
    # Solving
    sols, unks = solve(Vn.astype(float), dVn.astype(float), err_f, err_df, n=n, verbose=verbose)
    fl.ctx.prec = prec_save
    return sols, unks

# Debug : [-1, 1], [-1,0,0,0,0,1], [0,0,0,0,0,1]
def solve_polynomial_taylor(poly, n=None, k=None, t=73, verbose=False):
    prec_save = fl.ctx.prec
    fl.ctx.prec = 53
    if type(poly) == np.ndarray and poly.dtype == np.float64:
        #Vn = np.vectorize(fl.arb)(poly)
        Vn = None
        Vf = poly
        err_coeffs_f = float(0)
    else:
        Vn = np.array([to_arb(e) for e in poly])
        Vf = Vn.astype(float)
        #man, exp = np.abs(Va).max().upper().man_exp()
        #M = 2**(man.bit_length() + exp - 1)
        #Vn = Va/M
        err_coeffs_f = float(sum(e.rad() for e in Vn).upper())
    last = Vf.size-1
    while last >= 0 and Vf[last] == 0: last -= 1
    Vf = Vf[:last+1]
    #if Vn.size == 1: return np.array([]), np.array([])
    if Vf.size == 0: return np.array([[0, float('inf')]]), np.array([])
    Nt = min(Vf.size, int(np.sqrt(Vf.size*t))) # t=prec*2*ln 2, i.e. for machine prec 73 for 52*2*ln 2 
    #Nt = min(Vn.size, int(np.sqrt(Vn.size*60))) # 52*2*ln 2
    # global quantitities
    err_f = np.zeros(1)
    err_df = np.zeros(1)
    # solutions in [-1, 1]
    ## f conversion
    #Vf = Vn.astype(float)
    f = np.zeros(Nt)
    chebyshev(Vf, f, err_f)
    f = np.trim_zeros(f, 'b')
    err_f += err_coeffs_f
    ## df conversion
    Af = np.vectorize(fl.arb)(f)
    Adf = chebder(Af)
    df = Adf.astype(float)
    err_df = 2**np.log2(Vf.size-1)*err_f
    if Adf.size > 0:
        err_df += float(sum(e.rad() for e in Adf).upper())
    ## isolating
    solutions_id, unknowns_id = solve(f, df, err_f, err_df, n=n, k=k, verbose=verbose)
    # solutions in ]-oo, -1] U [1, +oo[
    ## f conversion
    Vf = np.ascontiguousarray(np.flip(Vf))
    f = np.zeros(Nt)
    chebyshev(Vf, f, err_f)
    f = np.trim_zeros(f, 'b')
    err_f += err_coeffs_f
    ## df conversion
    Af = np.vectorize(fl.arb)(f)
    Adf = chebder(Af)
    df = Adf.astype(float)
    err_df = 2**np.log2(Vf.size-1)*err_f
    if Adf.size > 0:
        err_df += float(sum(e.rad() for e in Adf).upper())
    ## isolating
    solutions_inv, unknowns_inv = solve(f, df, err_f, err_df, n=n, k=k, verbose=verbose)
    ## gathering
    solutions_rev = [1/fl.arb(*s) for s in solutions_inv if 0 not in fl.arb(*s)]
    solutions_rev = [[float(a.mid()), float(a.rad())] for a in solutions_rev]
    solution_inf = [fl.arb(*s) for s in solutions_inv if 0 in fl.arb(*s)]
    if len(solution_inf) > 0:
        #TODO: when lower bound to close to 0, nan ensues because fl.arb(1+l, 1-l) contains 0
        if Vn is None:
            Vn = np.vectorize(fl.arb)(np.flip(Vf))
        lower_bound = (abs(Vn[-1])/sum(np.abs(Vn[:-1]))).lower()
        if (Vf[1] < 0) == (Vf[0] > 0):
            solution_inf = 1/fl.arb.intersection(solution_inf[0],
                                      fl.arb((1.0+lower_bound)/2, (1.0-lower_bound)/2))
            solution_inf = [[float(solution_inf.mid()), float(solution_inf.rad())]]
        else:
            solution_inf = 1/fl.arb.intersection(solution_inf[0],
                                      fl.arb((-1.0-lower_bound)/2, (1.0-lower_bound)/2))
            solution_inf = [[float(solution_inf.mid()), float(solution_inf.rad())]]
    solutions = np.array(list(solutions_id) +  solutions_rev + solution_inf)
    unknowns_rev = [1/fl.arb(*s) for s in unknowns_inv]
    unknowns_rev = [[float(a.mid()), float(a.rad())] for a in unknowns_rev]
    unknowns = np.array(list(unknowns_id) +  unknowns_rev)
    fl.ctx.prec = prec_save
    return solutions, unknowns


