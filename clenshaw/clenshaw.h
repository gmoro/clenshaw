/****************************************************************************
        Copyright (C) 2019 Guillaume Moroz <guillaume.moroz@inria.fr>
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
                   http://www.gnu.org/licenses/
*****************************************************************************/

// Single precision horner
void s_horner(float* inout, int m, const float* coefficients, int n);
// Double precision horner
void d_horner(double* inout, int m, const double* coefficients, int n);
// Single precision balls horner
void sb_horner(float* inout_centers, float* inout_radii, int m, const float* coefficients, int n);
// Double precision balls horner
void db_horner(double* inout_centers, double* inout_radii, int m, const double* coefficients, int n);

// Single precision clenshaw
void s_clenshaw(float* inout, int m, const float* coefficients, int n);
// Double precision horner
void d_clenshaw(double* inout, int m, const double* coefficients, int n);
// Single precision balls clenshaw
void sb_clenshaw(float* inout_centers, float* inout_radii, int m, const float* coefficients, int n);
// Double precision balls clenshaw
void db_clenshaw(double* inout_centers, double* inout_radii, int m, const double* coefficients, int n);


// Double precision conversion to Chebyshev basis
void d_chebyshev(double* out, int m, const double* coefficients, int n);
// Double precision conversion to Chebyshev basis with error bound
void db_chebyshev(double* out, int m, double* radius, const double* coefficients, int n);

