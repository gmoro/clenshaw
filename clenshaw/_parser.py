#*****************************************************************************
#       Copyright (C) 2019 Guillaume Moroz <guillaume.moroz@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

import numpy as np
import fractions

def get_poly(filename, dtype=None, rescale=True):
    with open(filename, 'r') as input_file:
        Lc = []
        Le = []
        for line in input_file:
            if line.strip() and line.lstrip()[0] != '#':
                term = line.strip().split()
                dim = len(term)-1
                Lc.append(fractions.Fraction(term[0]))
                Le.append(tuple(int(t) for t in term[1:]))
            else:
                l = line.strip().split()
                if(len(Lc) == 0):
                    vars_list = l[1:len(l)]
    N = [max(e[i] for e in Le)+1 for i in range(dim)]
    if dtype is None:
        if list(map(float,Lc)) == Lc:
            dtype = np.float64
        else:
            dtype = object
    if dtype == object:
        poly = np.full(N, fractions.Fraction(0,1))
    else:
        poly = np.zeros(N, dtype=dtype)
    for i in range(len(Lc)):
        poly[Le[i]] = Lc[i]
    if rescale:
        poly = rescale_minmax(poly)
    return poly

def rescale_minmax(p):
    ap = np.abs(p)
    Ec = (int(ap.max()*ap[ap!=0].min()).bit_length()-1)//2
    if Ec > 0:
            p[p!=0] = p[p!=0]/2**Ec
    else:
            p[p!=0] = p[p!=0]*2**-Ec
    return p

