#*****************************************************************************
#       Copyright (C) 2019 Guillaume Moroz <guillaume.moroz@inria.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#                  http://www.gnu.org/licenses/
#*****************************************************************************

#from distutils.core import setup, Extension
from numpy.distutils.core import setup, Extension

libclenshaw = Extension('clenshaw._interface', ['clenshaw/clenshaw.c', 'clenshaw/interface.c'],
                         extra_compile_args = ['-std=c99', '-frounding-math',
                            '-O2', '-march=native', '-ftree-vectorize',
                            '-ffp-contract=fast', '-fopenmp',
                            '-Wno-unknown-pragmas', '-Wno-unused-function'],
                         extra_link_args = ['-fopenmp', '-lm'])

setup(name = 'clenshaw',
      version = '0.1',
      description = 'Interface to fast clenshaw evaluation',
      ext_modules = [libclenshaw],
      packages    = ['clenshaw'])


